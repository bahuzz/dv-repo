var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

var client;
var indexG;
var indexSearchTag;

$(document).ready(function() {
	client = algoliasearch('2EKKPMD9QX', '9e4211fdda3803490c04c1bdcf4b466d');
	indexG = client.initIndex('application_search_g');
	indexSearchTag = client.initIndex('tags');

	$('#login-btn').click(function(e){
		e.preventDefault();
		e.stopPropagation();
		$('#loginModal').arcticmodal();
	});
	$('#reg-btn').click(function(e){
		e.preventDefault();
		e.stopPropagation();
		$('.reg-tabs a[href="#reg-tab"]').tab('show');
		$('#regModal').arcticmodal();
	});

	$('#log-reg-btn').click(function(e){
		e.preventDefault();
		e.stopPropagation();
		$('.reg-tabs a[href="#reg-tab"]').tab('show');
		$('#loginModal').arcticmodal('close');
		$('#regModal').arcticmodal();
	});

	$('#usr-reg-btn').click(function(){
		$('.reg-tabs a[href="#reg-tab"]').tab('show');
		$('#regModal').arcticmodal();
	});

	$('#ven-reg-btn').click(function(){
		$('.reg-tabs a[href="#vendor-tab"]').tab('show');
		$('#regModal').arcticmodal();
	});

	$(document).on("click", ".btnMoreTags", function(e){
		e.preventDefault();
		e.stopPropagation();
		var tags_hide = $(this).parent().parent().find('.m-tags-hide');
		if($(tags_hide).css("display")=="none")
			$(tags_hide).show();
		else
			$(tags_hide).hide();
	});

	$(document).click(function(event) {
	    if (!$(event.target).closest(".m-tags-hide").length) 
	    { 
	    	$(".m-tags-hide").hide();
	    }
		if (!$(event.target).closest(".icon-block-app").length && !$(event.target).closest(".mini-icon-app").length) 
	    { 
	    	$(".sub-icon-block-app").hide();
	    }
	    if($("#sideSlideMenu").hasClass("sideSlideSlidePositive") && !$(event.target).closest("#sideSlideMenu").length && !$(event.target).closest("#sideSlideToggle").length)
	    {
	    	$("#sideSlideToggle").click();
	    }

	    event.stopPropagation();
  	});

  	$('.m-mig-block').hover(
	function(){},
	function(){
	  $(".m-tags-hide").hide();
	});

	$("#selectSearch").fcbkcomplete({
		site_url: _siteUrl,
        json_url: indexSearchTag,
        addontab: true,                   
        maxitems: 10,
        input_min_size: 0,
        height: 10,
        cache: false,
        newel: true,
        select_all_text: "select",
        complete_text: "",
        width:279, 
        filter_selected:true,   
        placeholder:"Type Keyword here ..."    
    });

	$(document).on("click",".btn-show-grid-app",function(){
		$(".btn-show-line-app").find("i").removeClass("active");
		$(this).find("i").addClass("active");
		$(".app").each(function(){
			$(this).removeClass("big-app");
		});
	});

	$(document).on("click",".btn-show-line-app",function(){
		$(".btn-show-grid-app").find("i").removeClass("active");
		$(this).find("i").addClass("active");
		$(".app").each(function(){
			$(this).addClass("big-app");
		});
	});

	$(document).on("click", ".btnFollowApp", function(){
		var appId = $(this).parent().parent().parent().parent().find(".app-id").val();
		var data = {
				'action': 'follow_app',
				'id' : appId
			};

		$.ajax({
			url:ajaxurl,
			data:data,
			type:'POST',
			success:function(data){
				if( data ) { 
					$('#pMessageText').html('Now you follow the application');
					$('#messageModal').arcticmodal();
				}	
			}
		});
	});

	$(document).on("click", ".btnUseApp", function(){
		var appId = $(this).parent().parent().parent().parent().find(".app-id").val();
		var data = {
				'action': 'using_app',
				'id' : appId
			};

		$.ajax({
			url:ajaxurl,
			data:data,
			type:'POST',
			success:function(data){
				if( data ) { 
					$('#pMessageText').html('The application has been added to your list');
					$('#messageModal').arcticmodal();
				}	
			}
		});
	});

	$(document).on("click", ".btnFollowBrand", function(e){
		e.preventDefault();
		e.stopPropagation();
		var brandId = $(this).attr("data-id");
		var data = {
				'action': 'follow_brand',
				'id' : brandId
			};

		$.ajax({
			url:ajaxurl,
			data:data,
			type:'POST',
			success:function(data){
				if( data ) { 
					$('#pMessageText').html('Now you follow the brand');
					$('#messageModal').arcticmodal();
				}	
			}
		});
	});
	
	$(document).on("click", ".btnFollowUser", function(e){
		e.preventDefault();
		e.stopPropagation();
		var userId = $(this).attr("data-id");
		var data = {
				'action': 'follow_user',
				'id' : userId
			};

		$.ajax({
			url:ajaxurl,
			data:data,
			type:'POST',
			success:function(data){
				if( data ) { 
					$('#pMessageText').html('Now you follow the user');
					$('#messageModal').arcticmodal();
				}	
			}
		});
	});

	if(isMobile.any())
	{
		$(document).on('click', '.item-icon-app .icon-app', showIconForApp);	

		$(document).on('click', '.item-mini-icon-app', showIconForApp);	

		$(document).on('click', '.btn-show-raiting-app', function(){
			$(this).parent().parent().find('.prod-stat-mobile').toggle();
			if($(this).parent().parent().find('.prod-stat-mobile').css('display')=='none')
			{
				$(this).parent().parent().parent().find('.prod-app-main').removeClass('degradation-background');
				$(this).parent().parent().parent().parent().find('.prod-tags').removeClass('degradation-background');	
				$(this).parent().parent().find('.prod-stat-mobile .stat-value').each(function(){
					$(this).css('width', '0%');
				});
			}
			else
			{	
				$(this).parent().parent().parent().find('.prod-app-main').addClass('degradation-background');
				if(!$(this).parent().parent().parent().parent().parent().hasClass('big-app'))
					$(this).parent().parent().parent().parent().find('.prod-tags').addClass('degradation-background');	
				$(this).parent().parent().find('.prod-stat-mobile .stat-value').each(function(){
					$(this).css('width', $(this).attr('data-pro')+'%');
				});
			}
		});

		$(document).on('click', '.btn-show-info-cat', function(){
			$(this).parent().find('.m-tags').toggle();	
			$(this).parent().find('.m-desc').toggle();		
		});

		$(document).on('click', '.btn-site-map', function(){
			var display = $(this).parent().find('ul:first').css('display');
			$(this).parent().parent().find('.li-map-site').each(function(){
				$(this).find('ul').hide();
				$(this).find('.fa-minus').removeClass('fa-minus').addClass('fa-plus');
			});
			if(display=='none')
			{
				$(this).find('.fa-plus:first').removeClass('fa-plus').addClass('fa-minus');
				$(this).parent().find('ul:first').show();
			}
		});
	}
	else
	{
		$('.fa').tooltip();

		$(document).on('mouseover', '.prod-block-cont', function() {
			$(this).find('.prod-app-main').addClass('degradation-background');
			if(!$(this).parent().parent().hasClass('big-app'))
				$(this).parent().find('.prod-tags').addClass('degradation-background');		
			$(this).find('.prod-stat .stat-value').each(function(){
				$(this).css('width', $(this).attr('data-pro')+'%');
			});
		});	
		$(document).on('mouseout', '.prod-block-cont', function() {
			$(this).find('.prod-app-main').removeClass('degradation-background');
			$(this).parent().find('.prod-tags').removeClass('degradation-background');		
			$(this).find('.prod-stat .stat-value').each(function(){
				$(this).css('width', '0%');
			});
		});	
	}
	
	function showIconForApp()
	{
		if($(this).parent().find(".sub-icon-block-app").css("display")=="block")
		{
			$(".sub-icon-block-app").hide();
		}
		else
		{
			$(".sub-icon-block-app").hide();
			$(this).parent().find(".sub-icon-block-app").show();
		}
	}

	$(document).on('click','.btnEmailApp',function(e){
		e.preventDefault();
		e.stopPropagation();
		
		$('#appEmailModal').arcticmodal();
	});	
	
	$('#login_form').validate({
        rules: {
        	login_username: "required",
		    login_password: "required"
        },
        onfocusout: function(element) {
            this.element(element);
        }  
    });

	$(document).on('click', '.btn-login-submit', function(e){
		e.preventDefault();
		e.stopPropagation();

		if($('#login_form').valid())
		{
			var data = {
				'action': 'authorization',
				'username' : $('#login-username').val(),
				'password' : $('#login-password').val()
			};

			$.ajax({
				url:ajaxurl,
				data:data,
				type:'POST',
				success:function(data){
					if( data ) { 
						
					}	
				}
			});
		}	
	});

    $('#register_user_form').validate({
        rules: {
        	reg_user_email: {
	    		required: true,
		        email: true
		    },
        	reg_user_username: "required",
        	reg_user_password: "required",
        	reg_user_password2: {
            	required: true,
            	equalTo: "#reg_user_password"
        	},
        	reg_user_chkIAgree: "required"
        },
        onfocusout: function(element) {
            this.element(element);
        }  
    });

	$(document).on('click', '.btn-register-submit', function(e){
		e.preventDefault();
		e.stopPropagation();

		if($('#register_user_form').valid())
		{
			$('#register_user_form').submit();
		}	
	});

	$('#report_app_form').validate({
        rules: {
            report: "required"
        },
        onfocusout: function(element) {
            this.element(element);
        }  
    });

    $(document).on('click', '.btn-report-app', function(){
		var appId = $(this).parent().parent().parent().parent().find(".app-id").val();
		$('#appReportModal').find(".app-id").val(appId);
		$('#appReportModal').arcticmodal();
	});

	$(document).on('click', '.btn-report-app-submit', function(e){
		e.preventDefault();
		e.stopPropagation();

		if($('#report_app_form').valid())
		{
			var appId = $(this).parent().find(".app-id").val();
			var data = {
				'action': 'report_app',
				'report' : $('#report-app').val(),
				'app' : appId
			};

			$.ajax({
				url:ajaxurl,
				data:data,
				type:'POST',
				success:function(data){
					if( data!='false' ) { 
						$('#report-app').val('');
						$('#appReportModal').arcticmodal('close');
						$('#pMessageText').html('Report submitted successfully');
						$('#messageModal').arcticmodal();
					}	
				}
			});
		}
	});
	
	$(document).on('click', '.btn-like-app', function(){		
		var data = {
			'action': 'like_app',
			'like' : $(this).attr("like"),
			'app' : $(this).attr("app")
		};

		$.ajax({
			url:ajaxurl,
			data:data,
			type:'POST',
			success:function(data){
				if( data!='false' ) { 
					$('#pMessageText').html('like:'+data);
					$('#messageModal').arcticmodal();
				}	
			}
		});
	});

	$(document).on('click','.close-popup',function(){
		$(this).parent().parent().parent().arcticmodal('close');
	});
/*
	$(document).on('click','.main-cat .m-mig-block',function(){
		$(this).parent().find('.site-map-category').toggle();
	});*/
});

function get_share_for_app()
{	
	$('.app-id-for-share').each(function(){
    	var id = 'divShareApp' + $(this).val();
		var data = {
      		'action': 'share_app',
      		'url' : $(this).parent().find('.app-permalink').val(),
      		'title' : $(this).parent().find('.app-title').val()
    	};

    	$.ajax({
      		url:ajaxurl,
      		data:data,
      		type:'POST',
      		success:function(data){
          		$('#'+id).html(data);
      		}
    	});
    });
	$('.fa').tooltip();
}